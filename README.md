# Gudang Ihwan

## Cara Install

### Masuk ke directory melalui terminal/CMD

`cd tugas2-sistem_gudang`

### Install Semua Package yang diperlukan

`npm i`

### Jalankan JSON-Server

`npm run server-json`

Untuk membuka Datanya : [Di localhost:3000](https://localhost:3000)

### Jalankan Angular Server

`ng serve`

Untuk membuka aplikasi : [Di localhost:4200](https://localhost:4200)