import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Merk } from './merk-data';
import { tap, catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MerkService {
  //private _baseUrl = 'https://my-json-server.typicode.com/lana404/sistem-gudang/pcs';
  private _baseUrl = 'http://localhost:3000/merk';

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  private handleError(err:any) {
    console.log(err);
    return throwError(err); 
  }

  getData(): Observable<Merk[]> {
    return this.httpClient.get<Merk[]>(this._baseUrl).pipe(
      catchError(this.handleError)
    )
  }

  getDataById(id:number): Observable<Merk> {
    return this.httpClient.get<Merk>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    )
  }

  hapusData(id:number): Observable<Merk> {
    return this.httpClient.delete<Merk>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  addData(merk:string): Observable<Merk> {
    return this.httpClient.post<Merk>(`${this._baseUrl}`, {merk: merk}).pipe(
      tap( data => console.log(data)),
      catchError(this.handleError)
    );
  }

  updateData(merk:any): Observable<Merk> {
    let searchParam = new URLSearchParams();
    searchParam.set('merk', merk.merk);
     
    let option = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    
    return this.httpClient.put<Merk>(`${this._baseUrl}/${merk.id}`, searchParam.toString(), option).pipe(
      catchError(this.handleError)
    );
  }
}
