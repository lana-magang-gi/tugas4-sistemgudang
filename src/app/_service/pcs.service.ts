import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { PCS } from './pcs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PcsService {
  //private _baseUrl = 'https://my-json-server.typicode.com/lana404/sistem-gudang/pcs';
  private _baseUrl = 'http://localhost:3000/pcs';

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  private handleError(err:any) {
    console.log(err);
    return throwError(err); 
  }

  getData(): Observable<PCS[]> {
    return this.httpClient.get<PCS[]>(this._baseUrl).pipe(
      catchError(this.handleError)
    )
  }

  getDataById(id:number): Observable<PCS> {
    return this.httpClient.get<PCS>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    )
  }

  hapusData(id:number): Observable<PCS> {
    return this.httpClient.delete<PCS>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  addData(data:any): Observable<PCS> {
    return this.httpClient.post<PCS>(`${this._baseUrl}`, data).pipe(
      catchError(this.handleError)
    );
  }

  updateData(data:any): Observable<PCS> {
    let searchParam = new URLSearchParams();
    searchParam.set('out', data.out);
    searchParam.set('masuk', data.masuk);
    searchParam.set('merk', data.merk);
     
    let option = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }
    
    return this.httpClient.put<PCS>(`${this._baseUrl}/${data.id}`, searchParam.toString(), option).pipe(
      catchError(this.handleError)
    );
  }
}
