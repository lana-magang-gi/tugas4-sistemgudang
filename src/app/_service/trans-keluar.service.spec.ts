import { TestBed } from '@angular/core/testing';

import { TransKeluarService } from './trans-keluar.service';

describe('TransKeluarService', () => {
  let service: TransKeluarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransKeluarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
