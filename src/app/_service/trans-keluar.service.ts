import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { TransMasuk } from './trans-masuk';

@Injectable({
  providedIn: 'root'
})
export class TransKeluarService {

  private _baseUrl = 'http://localhost:3000/trans-keluar';


  constructor(
    private httpClient: HttpClient
  ) { }

  private handleError(err:any) {
    console.log(err);
    return throwError(err); 
  }

  getData(): Observable<TransMasuk[]> {
    return this.httpClient.get<TransMasuk[]>(this._baseUrl).pipe(
      catchError(this.handleError)
    )
  }

  getDataById(id:number): Observable<TransMasuk> {
    return this.httpClient.get<TransMasuk>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    )
  }

  hapusData(id:number): Observable<TransMasuk> {
    return this.httpClient.delete<TransMasuk>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  addData(trans:any): Observable<TransMasuk> {
    return this.httpClient.post<TransMasuk>(`${this._baseUrl}`, trans).pipe(
      catchError(this.handleError)
    );
  }

  updateData(data:any): Observable<TransMasuk> {
    console.log(data);
    
    let searchParam = new URLSearchParams();
    searchParam.set('merk', data.merk);
    searchParam.set('date', data.date);
     
    let option = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }

    return this.httpClient.put<TransMasuk>(`${this._baseUrl}/${data.id}`, searchParam.toString(), option).pipe(
      catchError(this.handleError)
    );
  }
}
