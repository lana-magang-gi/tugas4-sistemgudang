import { TestBed } from '@angular/core/testing';

import { TransMasukService } from './trans-masuk.service';

describe('TransMasukService', () => {
  let service: TransMasukService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransMasukService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
