import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { tap, catchError, map} from 'rxjs/operators';
import { TransMasuk } from './trans-masuk';

@Injectable({
  providedIn: 'root'
})
export class TransMasukService {

  private _baseUrl = 'http://localhost:3000/trans-masuk';


  constructor(
    private httpClient: HttpClient
  ) { }

  private handleError(err:any) {
    console.log(err);
    return throwError(err); 
  }

  getData(): Observable<TransMasuk[]> {
    return this.httpClient.get<TransMasuk[]>(this._baseUrl).pipe(
      tap( data => console.log(data)),
      catchError(this.handleError)
    )
  }

  getDataById(id:number): Observable<TransMasuk> {
    return this.httpClient.get<TransMasuk>(`${this._baseUrl}/${id}`).pipe(
      tap( data => console.log(data) ),
      catchError(this.handleError)
    )
  }

  hapusData(id:number): Observable<TransMasuk> {
    return this.httpClient.delete<TransMasuk>(`${this._baseUrl}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  addData(trans:any): Observable<TransMasuk> {
    return this.httpClient.post<TransMasuk>(`${this._baseUrl}`, trans).pipe(
      tap( data => console.log(data)),
      catchError(this.handleError)
    );
  }

  updateData(data:any): Observable<TransMasuk> {
    let urlParams = new URLSearchParams();
    urlParams.set('merk', data.merk);
    urlParams.set('date', data.date);
     
    let option = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }

    return this.httpClient.put<TransMasuk>(`${this._baseUrl}/${data.id}`, urlParams.toString(), option).pipe(
      catchError(this.handleError)
    );
  }
}
