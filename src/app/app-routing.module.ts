import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './component/home/home.component';
import { MerkComponent } from './component/merk/merk.component';
import { MerkTambahComponent } from './component/merk-tambah/merk-tambah.component';
import { MerkEditComponent } from './component/merk-edit/merk-edit.component';
import { TransMasukComponent } from './component/trans-masuk/trans-masuk.component';
import { TransMasukEditComponent } from './component/trans-masuk-edit/trans-masuk-edit.component';
import { TransMasukTambahComponent } from './component/trans-masuk-tambah/trans-masuk-tambah.component';
import { PcsComponent } from './pcs/pcs.component';
import { PcsEditComponent } from './pcs-edit/pcs-edit.component';
import { PcsTambahComponent } from './pcs-tambah/pcs-tambah.component';
import { TransKeluarComponent } from './component/trans-keluar/trans-keluar.component';
import { TransKeluarEditComponent } from './component/trans-keluar-edit/trans-keluar-edit.component';
import { TransKeluarTambahComponent } from './component/trans-keluar-tambah/trans-keluar-tambah.component';

const routes: Routes = [
  { path: 'trans-masuk', component: TransMasukComponent },
  { path: 'trans-masuk/add', component: TransMasukTambahComponent},
  { path: 'trans-masuk/edit/:id', component: TransMasukEditComponent},
  { path: 'trans-keluar', component: TransKeluarComponent },
  { path: 'trans-keluar/add', component: TransKeluarTambahComponent },
  { path: 'trans-keluar/edit/:id', component: TransKeluarEditComponent },
  { path: 'home', component: HomeComponent },
  { path: 'pcs', component: PcsComponent },
  { path: 'pcs/add', component: PcsTambahComponent },
  { path: 'pcs/edit/:id', component: PcsEditComponent },
  { path: 'merk', component: MerkComponent }, 
  { path: 'merk/add', component: MerkTambahComponent },
  { path: 'merk/edit/:id', component: MerkEditComponent },
  { path: '',  redirectTo: 'home', pathMatch:'prefix' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
