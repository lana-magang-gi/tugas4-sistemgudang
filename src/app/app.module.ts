import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TransMasukComponent } from './component/trans-masuk/trans-masuk.component';
import { TransMasukEditComponent } from './component/trans-masuk-edit/trans-masuk-edit.component';
import { TransMasukTambahComponent } from './component/trans-masuk-tambah/trans-masuk-tambah.component';
import { TransKeluarComponent } from './component/trans-keluar/trans-keluar.component';
import { TransKeluarEditComponent } from './component/trans-keluar-edit/trans-keluar-edit.component';
import { TransKeluarTambahComponent } from './component/trans-keluar-tambah/trans-keluar-tambah.component';
import { MerkComponent } from './component/merk/merk.component';
import { HomeComponent } from './component/home/home.component';
import { MerkTambahComponent } from './component/merk-tambah/merk-tambah.component';
import { MerkEditComponent } from './component/merk-edit/merk-edit.component';
import { PcsComponent } from './pcs/pcs.component';
import { PcsTambahComponent } from './pcs-tambah/pcs-tambah.component';
import { PcsEditComponent } from './pcs-edit/pcs-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    TransMasukComponent,
    TransKeluarComponent,
    HomeComponent,
    MerkComponent,
    MerkTambahComponent,
    MerkEditComponent,
    TransMasukEditComponent,
    TransMasukTambahComponent,
    TransKeluarEditComponent,
    TransKeluarTambahComponent,
    PcsComponent,
    PcsTambahComponent,
    PcsEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
