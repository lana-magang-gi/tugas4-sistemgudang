import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { MerkService } from '../../_service/merk.service';



@Component({
  selector: 'app-merk-edit',
  templateUrl: './merk-edit.component.html',
  styleUrls: ['./merk-edit.component.css']
})

export class MerkEditComponent implements OnInit {
  public _formGroup: FormGroup;
  public _data;

  constructor(
    private formBuilder: FormBuilder,
    private merkService: MerkService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { 
    this._formGroup = this.formBuilder.group({
      id: [], merk:[]
    });  
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;

    this.merkService.getDataById(id).subscribe( 
      data => {
        this._formGroup = this.formBuilder.group({
          id: [ data.id, Validators.required],
          merk: [ data.merk, Validators.required]
        });  
      },
      () => {
        console.log('Gagal mendapatkan data');
      }
    );    
    this._formGroup.get('id').disable();
  }
  

  onSubmit() {
    this._data = this._formGroup.value;

    this.merkService.updateData(this._data).subscribe(
      () => {
        this.router.navigate(['merk']);
        console.log('Sukses mengupdate data');
      },
      () => {
        console.log('Gagal mengupdate Data');
      }
    )
  }
}
