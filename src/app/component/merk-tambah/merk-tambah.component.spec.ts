import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerkTambahComponent } from './merk-tambah.component';

describe('MerkTambahComponent', () => {
  let component: MerkTambahComponent;
  let fixture: ComponentFixture<MerkTambahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerkTambahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerkTambahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
