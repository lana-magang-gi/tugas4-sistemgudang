import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MerkService } from '../../_service/merk.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-merk-tambah',
  templateUrl: './merk-tambah.component.html',
  styleUrls: ['./merk-tambah.component.css']
})

export class MerkTambahComponent implements OnInit {
  public _formGroup: FormGroup;
  public _position: string;
  private _data;

  constructor(
    private formBuilder: FormBuilder,
    private merkService: MerkService,
    private router: Router
  ) {
    this._position = location.pathname.substr(1);
  }

  ngOnInit(): void {
    if (this._position == 'merk/add') {
      this._formGroup = this.formBuilder.group({
        merk: ['', Validators.required]
      });  
    }
  }

  onSubmit():void {
    this._data = this._formGroup.value.merk;
    this.merkService.addData(this._data)
    .subscribe(
      () => {
        this.router.navigate(['/merk']);
      }, 
      () => {
        console.log('Gagal Menambahkan Data. ');
      }
    )
  }

}
