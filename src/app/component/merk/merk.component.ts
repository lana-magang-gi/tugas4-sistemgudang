import { Component, OnInit } from '@angular/core';
import { MerkService } from '../../_service/merk.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-merk',
  templateUrl: 'merk.component.html',
  styleUrls: ['./merk.component.css']
})
export class MerkComponent implements OnInit {
  public _data;

  constructor(
    private merkService: MerkService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.merkService.getData().subscribe( 
      data => {
        this._data = data;
      },
      () => {
        console.log('Gagal Memuat Data');
      }
    );
  }
  
  editData(id: number) {
    this.router.navigate(['merk/edit', id])
  }

  hapusData(id: number) {
    this.merkService.hapusData(id).subscribe(
      () => {
        console.log('Sukses menghapus Data');
      }, 
      () => {
        console.log('Gagal Menghapus Data');
      }
    ).unsubscribe();

    this.getData();
  }
}
