import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransKeluarEditComponent } from './trans-keluar-edit.component';

describe('TransKeluarEditComponent', () => {
  let component: TransKeluarEditComponent;
  let fixture: ComponentFixture<TransKeluarEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransKeluarEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransKeluarEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
