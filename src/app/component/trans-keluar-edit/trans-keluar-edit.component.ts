import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TransKeluarService } from '../../_service/trans-keluar.service';

@Component({
  selector: 'app-trans-keluar-edit',
  templateUrl: './trans-keluar-edit.component.html',
  styleUrls: ['./trans-keluar-edit.component.css']
})
export class TransKeluarEditComponent implements OnInit {
  public _formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private transKelService: TransKeluarService
  ) { 
    this._formGroup = this.formBuilder.group({
      id: ['', Validators.required],
      merk: ['', Validators.required],
      date: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;

    this.transKelService.getDataById(id).subscribe(
      data => {
        this._formGroup = this.formBuilder.group({
          id: [data.id],
          merk: [data.merk],
          date: [data.date]
        });
      },
      () => {
        console.log('Gagal mendapatkan data');
      }
    )
    this._formGroup.get('id').disable(); 
  }

  onSubmit(): void {
    let data = this._formGroup.value;

    this.transKelService.updateData(data).subscribe(
      () => {
        this.router.navigate(['/trans-keluar'])
      },
      () => {
        console.log('Gagal mengupdate data');
      }
    )
  }
}
