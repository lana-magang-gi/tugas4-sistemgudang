import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransKeluarTambahComponent } from './trans-keluar-tambah.component';

describe('TransKeluarTambahComponent', () => {
  let component: TransKeluarTambahComponent;
  let fixture: ComponentFixture<TransKeluarTambahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransKeluarTambahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransKeluarTambahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
