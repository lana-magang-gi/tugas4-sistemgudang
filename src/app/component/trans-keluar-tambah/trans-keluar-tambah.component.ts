import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TransKeluarService } from '../../_service/trans-keluar.service';

@Component({
  selector: 'app-trans-keluar-tambah',
  templateUrl: './trans-keluar-tambah.component.html',
  styleUrls: ['./trans-keluar-tambah.component.css']
})
export class TransKeluarTambahComponent implements OnInit {
  public _formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private transKelService: TransKeluarService
  ) {     
    this._formGroup = this.formBuilder.group({
      merk: ['', Validators.required],
      date: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    
  }

  onSubmit(): void {
    let data = this._formGroup.value;

    this.transKelService.addData(data).subscribe(
      () => {
        console.log('Sukses menambahkan data');
        this.router.navigate(['/trans-keluar']);
      },
      () => {
        console.log('Gagal Menambahkan data');
      }
    )    
  }

}
