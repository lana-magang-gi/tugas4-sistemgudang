import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransKeluarComponent } from './trans-keluar.component';

describe('TransKeluarComponent', () => {
  let component: TransKeluarComponent;
  let fixture: ComponentFixture<TransKeluarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransKeluarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransKeluarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
