import { Component, OnInit } from '@angular/core';
import { TransKeluarService } from '../../_service/trans-keluar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trans-keluar',
  templateUrl: './trans-keluar.component.html',
  styleUrls: ['./trans-keluar.component.css']
})
export class TransKeluarComponent implements OnInit {
  public _data: any;

  constructor(
    private transKeluarService: TransKeluarService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData():void {
    this.transKeluarService.getData().subscribe(
      data => {
        this._data = data;
        console.log('Sukses Mengambil Data');
      },
      () => {
        console.log('Gagal Mengambil Data');
      }
    )
  }

  hapusData(id:number) {
    this.transKeluarService.hapusData(id).subscribe(
      () => {
        console.log('Sukses Menghapus Data');
        this.getData();
      },
      () => {
        console.error('Gagal Menghapus Data');
      }
    )
  }

  editData(id:number) {
    this.router.navigate(['/trans-keluar/edit', id]);
  }
}
