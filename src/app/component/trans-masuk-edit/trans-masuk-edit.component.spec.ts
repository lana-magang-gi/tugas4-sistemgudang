import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransMasukEditComponent } from './trans-masuk-edit.component';

describe('TransMasukEditComponent', () => {
  let component: TransMasukEditComponent;
  let fixture: ComponentFixture<TransMasukEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransMasukEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransMasukEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
