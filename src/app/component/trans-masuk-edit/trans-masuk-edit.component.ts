import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TransMasukService } from '../../_service/trans-masuk.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-trans-masuk-edit',
  templateUrl: './trans-masuk-edit.component.html',
  styleUrls: ['./trans-masuk-edit.component.css']
})
export class TransMasukEditComponent implements OnInit {
  public _formGroup: FormGroup;
  private _data:any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private transMasService: TransMasukService,
    private formBuilder: FormBuilder
  ) { 
    this._formGroup = this.formBuilder.group({
      id: ['', Validators.required],
      merk: ['', Validators.required],
      date: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;

    this.transMasService.getDataById(id).subscribe(
      data => {
        this._formGroup = this.formBuilder.group({
          id: [data.id],
          merk: [data.merk],
          date: [data.date]
        })
      },
      () => {
        console.log('Data tidak tersedia');
      }
    );

    this._formGroup.get('id').disable();    
  }

  onSubmit(): void {
    this._data = this._formGroup.value;

    this.transMasService.updateData(this._data).subscribe(
      () => {
        this.router.navigate(['/trans-masuk']);
      },
      () => {
        console.log('Gagal Mengupdate Data');
      }
    )
  }

}
