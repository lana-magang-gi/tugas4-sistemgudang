import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransMasukTambahComponent } from './trans-masuk-tambah.component';

describe('TransMasukTambahComponent', () => {
  let component: TransMasukTambahComponent;
  let fixture: ComponentFixture<TransMasukTambahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransMasukTambahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransMasukTambahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
