import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TransMasukService } from '../../_service/trans-masuk.service';

@Component({
  selector: 'app-trans-masuk-tambah',
  templateUrl: './trans-masuk-tambah.component.html',
  styleUrls: ['./trans-masuk-tambah.component.css']
})
export class TransMasukTambahComponent implements OnInit {
  public _formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private transMasService: TransMasukService
  ) {     
    this._formGroup = this.formBuilder.group({
      merk: ['', Validators.required],
      trans: ['', Validators.required],
      date: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    
  }

  onSubmit(): void {
    let data = this._formGroup.value;

    this.transMasService.addData(data).subscribe(
      () => {
        console.log('Sukses menambahkan data');
        this.router.navigate(['/trans-masuk']);
      },
      () => {
        console.log('Gagal Menambahkan data');
      }
    )    
  }

}
