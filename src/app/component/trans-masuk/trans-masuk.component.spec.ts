import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransMasukComponent } from './trans-masuk.component';

describe('TransMasukComponent', () => {
  let component: TransMasukComponent;
  let fixture: ComponentFixture<TransMasukComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransMasukComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransMasukComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
