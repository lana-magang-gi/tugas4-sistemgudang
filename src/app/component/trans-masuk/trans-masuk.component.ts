import { Component, OnInit } from '@angular/core';
import { TransMasukService } from '../../_service/trans-masuk.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trans-masuk',
  templateUrl: './trans-masuk.component.html',
  styleUrls: ['./trans-masuk.component.css']
})
export class TransMasukComponent implements OnInit {
  public _data;

  constructor(
    private transMasService: TransMasukService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.transMasService.getData().subscribe( 
      data => {
        this._data = data;
      },
      () => {
        console.log('Gagal Memuat Data');
      }
    );
  }

  editData(id: number) {
    this.router.navigate(['trans-masuk/edit', id]);
  }

  hapusData(id: number) {
    this.transMasService.hapusData(id).subscribe(
      () => {
        console.log('Sukses menghapus Data');
      }, 
      () => {
        console.log('Gagal Menghapus Data');
      }
    ).unsubscribe();

    this.getData();
  }

}
