import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcsEditComponent } from './pcs-edit.component';

describe('PcsEditComponent', () => {
  let component: PcsEditComponent;
  let fixture: ComponentFixture<PcsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
