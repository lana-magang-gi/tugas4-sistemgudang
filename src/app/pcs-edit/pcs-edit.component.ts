import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PcsService } from '../_service/pcs.service';
import { LifecycleHooks } from '@angular/compiler/src/lifecycle_reflector';

@Component({
  selector: 'app-pcs-edit',
  templateUrl: './pcs-edit.component.html',
  styleUrls: ['./pcs-edit.component.css']
})
export class PcsEditComponent implements OnInit {
  public _formGroup: FormGroup;
  public _data: any;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private pcsService: PcsService
  ) { 
    this._formGroup = this.formBuilder.group({
      id: [], merk: [], masuk: [], out: []
    })
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.params.id;

    this.pcsService.getDataById(id).subscribe(
      data => {
        this._formGroup = this.formBuilder.group({
          id: [data.id, Validators.required], 
          merk: [data.merk, Validators.required], 
          masuk: [data.masuk, Validators.required], 
          out: [data.out, Validators.required]
        })
        console.log('Berhasil memuat data');
      },
      () => {
        console.log('Gagal Memuat Data');
      }
    )
    this._formGroup.get('id').disable();
  }

  onSubmit(): void {
    let data = this._formGroup.value;
    
    this.pcsService.updateData(data).subscribe(
      () => {
        this.router.navigate(['/pcs']);
        console.log('Sukses mengupdate data');
      },
      () => {
        console.log('Gagal mengupdate data');
      }
    )
  }

}
