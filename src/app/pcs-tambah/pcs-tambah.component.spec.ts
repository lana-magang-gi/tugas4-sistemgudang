import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcsTambahComponent } from './pcs-tambah.component';

describe('PcsTambahComponent', () => {
  let component: PcsTambahComponent;
  let fixture: ComponentFixture<PcsTambahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcsTambahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcsTambahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
