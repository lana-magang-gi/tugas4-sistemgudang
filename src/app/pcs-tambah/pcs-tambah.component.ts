import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PcsService } from '../_service/pcs.service';

@Component({
  selector: 'app-pcs-tambah',
  templateUrl: './pcs-tambah.component.html',
  styleUrls: ['./pcs-tambah.component.css']
})
export class PcsTambahComponent implements OnInit {
  public _formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private pcsService: PcsService
  ) {
    this._formGroup = this.formBuilder.group({
      merk: ['', Validators.required], 
      masuk: ['', Validators.required], 
      out: ['', Validators.required]
    });
   }

  ngOnInit(): void {
  }

  onSubmit(): void {
    let data = this._formGroup.value;
    
    this.pcsService.addData(data).subscribe(
      () => {
        this.router.navigate(['/pcs']);
      },
      () => {
        console.log('Gagal tambah data');
      }
    );
  }

}
