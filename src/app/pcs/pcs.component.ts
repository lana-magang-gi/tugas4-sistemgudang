import { Component, OnInit } from '@angular/core';
import { PcsService } from '../_service/pcs.service';
import { Router } from '@angular/router';
import { LifecycleHooks } from '@angular/compiler/src/lifecycle_reflector';

@Component({
  selector: 'app-pcs',
  templateUrl: './pcs.component.html',
  styleUrls: ['./pcs.component.css']
})
export class PcsComponent implements OnInit {
  public _data:any;

  constructor(
    private pcsService: PcsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.pcsService.getData().subscribe(
      data => {
        this._data = data;
      },
      () => {
        console.log('Gagal Mengambil data');
      }
    )
  }

  editData(id: number) {
    this.router.navigate(['/pcs/edit', id]);
  }

  hapusData(id: number) {
    this.pcsService.hapusData(id).subscribe(
      () => {
        console.log('Sukses menghapus data');
        this.getData();
      },
      () => {
        console.log('Hapus Data gagal');
      }
    )
  }

}
